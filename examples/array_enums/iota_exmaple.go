package main

import (
	"fmt"
)

func main() {

	// Array
	//nums := []int{1, 2, 3, 4, 5, 6, 7}

	palabra := "palabra"

	// Los indices empiezan en 0

	sub_palabras := palabra[5:]

	fmt.Println(sub_palabras)

}
