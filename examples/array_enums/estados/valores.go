package estados

type Estado int

const (
	Activo Estado = iota
	Inactivo
	Pendiente
	Eliminado
)
