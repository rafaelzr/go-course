# Enums y Slice 

## Enums

¿Qué son las Enums?
Las Enums, o enumeraciones, son un conjunto de constantes con nombre que representan un conjunto de valores posibles para un tipo de datos específico. En Go, no hay un tipo de datos específico para Enums como en algunos otros lenguajes, pero podemos simular Enums usando el tipo de datos int y constantes.
Implementación de Enums en Go
En Go, se pueden crear Enums usando constantes con un tipo de dato específico. Aquí hay un ejemplo básico de cómo podrías crear Enums:
```Golang
package main

import "fmt"

// Definir un tipo Enum usando constantes y un tipo de dato subyacente
type Estado int

const (
    Activo Estado = iota
    Inactivo
    Pendiente
    Eliminado
)
```



Sobre las Enums String
En Go, no hay un tipo de dato Enum directo para representar una lista de strings como en algunos otros lenguajes. Sin embargo, se pueden simular Enums de strings utilizando constantes y un tipo subyacente específico.
Aquí te muestro un ejemplo de cómo podrías implementar Enums de strings en Go:

```Golang
package main

import "fmt"

// Definir un tipo EnumString como un string
type EstadoString string

const (
    Activo    EstadoString = "Activo"
    Inactivo  EstadoString = "Inactivo"
    Pendiente EstadoString = "Pendiente"
    Eliminado EstadoString = "Eliminado"
)
```


En este caso, hemos creado un tipo EstadoString que usa strings como valores para representar diferentes estados. Cada constante definida tiene asignado un string específico que representa un estado.
Puedes usar estas constantes en funciones o estructuras de manera similar a como se usarían Enums de tipos numéricos:

```Golang
func obtenerEstadoMensajeString(estado EstadoString) string {
    switch estado {
    case Activo:
        return "El estado está activo"
    case Inactivo:
        return "El estado está inactivo"
    case Pendiente:
        return "El estado está pendiente"
    case Eliminado:
        return "El estado está eliminado"
    default:
        return "Estado desconocido"
    }
}
```

## Slice

En Go, a diferencia de los Arrays que poseen un tamaño fijo, los Slices representan una secuencia de tamaño variable de elementos del mismo tipo, es decir poseen un tamaño dinámico. 

```Golang
package main

import (
	"fmt"
)

func main() {
	// Array
	cursos := [6]string{"Go","CSS","Javascript","Vue","Bases de Datos"}
	// Slice
	var frontend []string = cursos[1:4]
	fmt.Println(frontend)
}
```


## Ejecicio

Crear una CRUD de tipo Cliente

Cliete{
    name
    id
}

- GetAll
- Get

Usar una Array para almacenar los objetos

Puedes usar cualquier Framework explicado

La respuesta seaun String