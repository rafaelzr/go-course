package calendars

type Calendar string

const (
	REGULAR      Calendar = "regular"
	BUSINESS_DAY          = "business-day"
)
