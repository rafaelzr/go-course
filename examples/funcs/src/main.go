package main

import (
	"fmt"

	"gitlab.com/fairpla/funcs/src/calc"
)

func suma_local(x, y int) int {
	return x + y
}

func main() {
	_x := 5
	_y := 8
	fmt.Printf("La suma de %d + %d = %d\n", _x, _y, suma_local(_x, _y))
	fmt.Printf("La multiplicacion de %d + %d = %d\n", _x, _y, calc.Multiplicacion(_x, _y))
}
