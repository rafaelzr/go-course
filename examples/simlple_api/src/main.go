package main

import (
	"github.com/gofiber/fiber/v2"
)

/*
## Ejecicio

Crear una CRUD de tipo Cliente

Cliete{
    name
    id
}
- GetAll
- Get

Usar una Array para almacenar los objetos

Puedes usar cualquier Framework explicado

La respuesta seaun String
*/

type Cliente struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

type ResultList struct {
	Errors  []string  `json:"errors"`
	Clients []Cliente `json:"results"`
}

type ResultClient struct {
	Errors  []string `json:"errors"`
	Clients Cliente  `json:"client"`
}

var clients = []Cliente{
	{
		Name: "Rafa",
		ID:   "d3bb9675-67fc-4e9e-9389-46b3201765e0",
	},
	{
		Name: "Marlen",
		ID:   "0cef1bbe-2e4a-414b-982c-4e733b857de4",
	},
	{
		Name: "Roni",
		ID:   "21a53710-126b-43d7-a897-5ea6e4331a13",
	},
}

func HelloWorld(c *fiber.Ctx) error {
	return c.SendString("Hello, World!")
}

func GetAllClients(c *fiber.Ctx) error {
	response := ResultList{
		Errors:  nil,
		Clients: clients,
	}
	return c.JSON(response)
}

func GetClient(c *fiber.Ctx) error {

	paramID := c.Params("id")

	for _, client := range clients {
		if client.ID == paramID {
			response := ResultClient{
				Errors:  nil,
				Clients: client,
			}
			return c.JSON(response)
		}
	}

	response := ResultList{
		Errors:  []string{"NotFound Client"},
		Clients: nil,
	}

	return c.Status(fiber.StatusNotFound).JSON(response)
}

func main() {
	app := fiber.New()

	app.Get("/", HelloWorld)

	app.Get("/clients", GetAllClients)

	app.Get("/clients/:id", GetClient)

	app.Listen(":4000")
}
