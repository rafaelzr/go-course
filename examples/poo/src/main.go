package main

import (
	"fmt"
)

type Contract struct {
	ID              string
	repousal_amount float32
	status          string
}

func (c Contract) print(num int) (string, error) {

	fmt.Printf("El contracto con ID: %s\n", c.ID)
	fmt.Printf("Tiene repousal amount: %f\n", c.repousal_amount)
	fmt.Printf("Tiene el status: '%s'\n", c.status)
	fmt.Println(num)
	return "Hola", fmt.Errorf("Hubo un error")
}

func PrintContract(c Contract) string {
	c.print(3)
	return "\"fsfsf"
}

func DeactivateByCopy(c Contract) {
	c.status = "deactivate"
	c.print(5)
}

func DeactivateByReference(c *Contract) {
	c.status = "deactivate"
}

func main() {
	contract := Contract{ID: "jdsasdasdak", repousal_amount: 23, status: "active"}

	PrintContract(contract)

	DeactivateByCopy(contract)
	PrintContract(contract)

	DeactivateByReference(&contract)
	PrintContract(contract)
}
