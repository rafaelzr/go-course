package main

import "fmt"

func flow_control_with_switch() {
	languages := []string{"español", "français", "english", "italiano"}

	for _, language := range languages {
		fmt.Printf("Saludos en %s: ", language)

		switch language {
		case "español":
			fmt.Println("¡Hola, mundo!")
		case "français":
			fmt.Println("Bonjour, le monde!")
		case "english":
			fmt.Println("Hello, World!")
		default:
			fmt.Println("No se reconoce el idioma.")
		}
	}
}

func flow_control_with_if() {
	language := "español"

	if language == "español" {
		fmt.Println("¡Hola, mundo!")
	} else if language == "français" {
		fmt.Println("Bonjour, le monde!")
	} else {
		fmt.Println("Hello, World!")
	}
}

func main() {
	fmt.Println("====> flow_control_with_switch")
	flow_control_with_switch()

	fmt.Println("====> flow_control_with_if")
	flow_control_with_if()
}
