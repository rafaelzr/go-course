package client

type ResultList struct {
	Errors  []string  `json:"errors"`
	Clients []Cliente `json:"results"`
}

type ResultClient struct {
	Errors  []string `json:"errors"`
	Clients Cliente  `json:"client"`
}
