package client

import "gorm.io/gorm"

type Cliente struct {
	gorm.Model
	Name string `json:"name"`
}
