package client

import (
	"fmt"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/fairplay/api_gorm/src/core"
)

var clients = []Cliente{
	{
		Name: "Rafa",
	},
	{
		Name: "Marlen",
	},
	{
		Name: "Roni",
	},
}

func GetAllClients(c *fiber.Ctx) error {
	response := ResultList{
		Errors:  nil,
		Clients: clients,
	}
	return c.JSON(response)
}

func GetClient(c *fiber.Ctx) error {

	paramID, _ := strconv.ParseUint(c.Params("id"), 10, 64)

	var client Cliente
	var response ResultList

	err := core.Session.First(&client, "id = ?", paramID).Error

	if err != nil {
		response = ResultList{
			Errors:  []string{fmt.Sprintf("Error to find Client: %e", err)},
			Clients: nil,
		}
	}

	if client.ID != uint(paramID) {
		response = ResultList{
			Errors:  []string{"Error not find Client"},
			Clients: nil,
		}
	} else {
		response = ResultList{
			Errors:  []string{},
			Clients: []Cliente{client},
		}
	}

	return c.Status(fiber.StatusNotFound).JSON(response)

}
