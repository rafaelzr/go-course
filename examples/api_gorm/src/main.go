package main

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/fairplay/api_gorm/src/api/v1/client"
	"gitlab.com/fairplay/api_gorm/src/core"
)

func HelloWorld(c *fiber.Ctx) error {
	return c.SendString("Hello, World!")
}

func main() {

	core.SetupDB()

	// core.AutoMigrate()

	// core.InserData()

	app := fiber.New()

	app.Get("/", HelloWorld)

	app.Get("/clients", client.GetAllClients)

	app.Get("/clients/:id", client.GetClient)

	app.Listen(":4000")
}
