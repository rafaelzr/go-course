package core

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var Session *gorm.DB
var err error

func SetupDB() {

	URL_SQLite := "my_database.db"
	Session, err = gorm.Open(sqlite.Open(URL_SQLite), &gorm.Config{})
	// Example with Postgres
	// Session, err = gorm.Open(postgres.Open(URL_POSTGRESQL), &gorm.Config{})
	if err != nil {
		print(err)
		panic(err)
	}
}

/*
func AutoMigrate() {
	Session.AutoMigrate(&client.Cliente{})
}

func InserData() {
	Session.Create(&client.Cliente{Name: "Rafa"})
	Session.Create(&client.Cliente{Name: "Roni"})
	Session.Create(&client.Cliente{Name: "Marlen"})
}
*/
